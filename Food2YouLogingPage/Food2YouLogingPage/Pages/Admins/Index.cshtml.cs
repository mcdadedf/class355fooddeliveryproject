﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Food2YouLogingPage.Data;

namespace Food2YouLogingPage.Pages.Admins
{
    public class IndexModel : PageModel
    {
        private readonly Food2YouLogingPage.Data.ApplicationDbContext _context;

        public IndexModel(Food2YouLogingPage.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Admin> Admin { get;set; }

        public async Task OnGetAsync()
        {
            Admin = await _context.Admins.ToListAsync();
        }
    }
}
