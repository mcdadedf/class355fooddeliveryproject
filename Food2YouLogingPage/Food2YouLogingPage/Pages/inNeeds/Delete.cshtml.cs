﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Food2YouLogingPage.Data;

namespace Food2YouLogingPage.Pages.inNeeds
{
    public class DeleteModel : PageModel
    {
        private readonly Food2YouLogingPage.Data.ApplicationDbContext _context;

        public DeleteModel(Food2YouLogingPage.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public inNeed inNeed { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            inNeed = await _context.InNeed.FirstOrDefaultAsync(m => m.ID == id);

            if (inNeed == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            inNeed = await _context.InNeed.FindAsync(id);

            if (inNeed != null)
            {
                _context.InNeed.Remove(inNeed);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
