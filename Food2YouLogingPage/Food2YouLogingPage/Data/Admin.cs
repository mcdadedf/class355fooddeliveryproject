﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Food2YouLogingPage.Data
{
    public class Admin
    {
        public int ID { get; set; }
        public IdentityUser WebAdmin { get; set; }
        public inNeed inNeed { get; set; }
        public string fNmae { get; set; }
        public string lName { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string licenseNum { get; set; }
        public string carMake { get; set; }
        public string carModel { get; set; }
        public string carPlate { get; set; }
    }
}
