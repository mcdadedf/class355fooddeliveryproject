﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Food2YouLogingPage.Data
{
    public class inNeed
    {
        public int ID { get; set; }
        public string fNmae { get; set; }
        public string lName { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public int numberOfPeople { get; set; }
        public string phone { get; set; }
        public string allergies { get; set; }
        public string DayOfTheWeek { get; set; }

    }
}
