﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Food2YouLogingPage.Data.Migrations
{
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Admin_AspNetUsers_WebAdminId",
                table: "Admin");

            migrationBuilder.DropForeignKey(
                name: "FK_Admin_inNeed_inNeedID",
                table: "Admin");

            migrationBuilder.DropPrimaryKey(
                name: "PK_inNeed",
                table: "inNeed");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Admin",
                table: "Admin");

            migrationBuilder.RenameTable(
                name: "inNeed",
                newName: "InNeed");

            migrationBuilder.RenameTable(
                name: "Admin",
                newName: "Admins");

            migrationBuilder.RenameIndex(
                name: "IX_Admin_inNeedID",
                table: "Admins",
                newName: "IX_Admins_inNeedID");

            migrationBuilder.RenameIndex(
                name: "IX_Admin_WebAdminId",
                table: "Admins",
                newName: "IX_Admins_WebAdminId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InNeed",
                table: "InNeed",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Admins",
                table: "Admins",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Admins_AspNetUsers_WebAdminId",
                table: "Admins",
                column: "WebAdminId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Admins_InNeed_inNeedID",
                table: "Admins",
                column: "inNeedID",
                principalTable: "InNeed",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Admins_AspNetUsers_WebAdminId",
                table: "Admins");

            migrationBuilder.DropForeignKey(
                name: "FK_Admins_InNeed_inNeedID",
                table: "Admins");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InNeed",
                table: "InNeed");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Admins",
                table: "Admins");

            migrationBuilder.RenameTable(
                name: "InNeed",
                newName: "inNeed");

            migrationBuilder.RenameTable(
                name: "Admins",
                newName: "Admin");

            migrationBuilder.RenameIndex(
                name: "IX_Admins_inNeedID",
                table: "Admin",
                newName: "IX_Admin_inNeedID");

            migrationBuilder.RenameIndex(
                name: "IX_Admins_WebAdminId",
                table: "Admin",
                newName: "IX_Admin_WebAdminId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_inNeed",
                table: "inNeed",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Admin",
                table: "Admin",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Admin_AspNetUsers_WebAdminId",
                table: "Admin",
                column: "WebAdminId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Admin_inNeed_inNeedID",
                table: "Admin",
                column: "inNeedID",
                principalTable: "inNeed",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
