﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Food2YouLogingPage.Data.Migrations
{
    public partial class addressFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "adress",
                table: "Admins");

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "Admins",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "address",
                table: "Admins");

            migrationBuilder.AddColumn<string>(
                name: "adress",
                table: "Admins",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
