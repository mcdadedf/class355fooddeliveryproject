﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Food2YouLogingPage.Data.Migrations
{
    public partial class DaysofWeekCHange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Friday",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "Monday",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "Saturday",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "Sunday",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "Thurday",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "Tuesday",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "Wednesday",
                table: "InNeed");

            migrationBuilder.AddColumn<string>(
                name: "DayOfTheWeek",
                table: "InNeed",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DayOfTheWeek",
                table: "InNeed");

            migrationBuilder.AddColumn<bool>(
                name: "Friday",
                table: "InNeed",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Monday",
                table: "InNeed",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Saturday",
                table: "InNeed",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sunday",
                table: "InNeed",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Thurday",
                table: "InNeed",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Tuesday",
                table: "InNeed",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Wednesday",
                table: "InNeed",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
