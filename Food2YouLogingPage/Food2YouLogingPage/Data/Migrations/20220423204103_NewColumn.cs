﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Food2YouLogingPage.Data.Migrations
{
    public partial class NewColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "carMake",
                table: "Admins",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "carModel",
                table: "Admins",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "carPlate",
                table: "Admins",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "licenseNum",
                table: "Admins",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "phone",
                table: "Admins",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "carMake",
                table: "Admins");

            migrationBuilder.DropColumn(
                name: "carModel",
                table: "Admins");

            migrationBuilder.DropColumn(
                name: "carPlate",
                table: "Admins");

            migrationBuilder.DropColumn(
                name: "licenseNum",
                table: "Admins");

            migrationBuilder.DropColumn(
                name: "phone",
                table: "Admins");
        }
    }
}
