﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Food2YouLogingPage.Data.Migrations
{
    public partial class needyNewFeilds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "adress",
                table: "InNeed");

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "InNeed",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "allergies",
                table: "InNeed",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfPeople",
                table: "InNeed",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "phone",
                table: "InNeed",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "address",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "allergies",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "numberOfPeople",
                table: "InNeed");

            migrationBuilder.DropColumn(
                name: "phone",
                table: "InNeed");

            migrationBuilder.AddColumn<string>(
                name: "adress",
                table: "InNeed",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
