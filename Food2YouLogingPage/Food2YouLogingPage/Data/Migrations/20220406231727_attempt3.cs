﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Food2YouLogingPage.Data.Migrations
{
    public partial class attempt3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.CreateTable(
                name: "inNeed",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    fNmae = table.Column<string>(nullable: true),
                    lName = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    adress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_inNeed", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Admin",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WebAdminId = table.Column<string>(nullable: true),
                    inNeedID = table.Column<int>(nullable: true),
                    fNmae = table.Column<string>(nullable: true),
                    lName = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    adress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admin", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Admin_AspNetUsers_WebAdminId",
                        column: x => x.WebAdminId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Admin_inNeed_inNeedID",
                        column: x => x.inNeedID,
                        principalTable: "inNeed",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Admin_WebAdminId",
                table: "Admin",
                column: "WebAdminId");

            migrationBuilder.CreateIndex(
                name: "IX_Admin_inNeedID",
                table: "Admin",
                column: "inNeedID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Admin");

            migrationBuilder.DropTable(
                name: "inNeed");

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.ID);
                });
        }
    }
}
